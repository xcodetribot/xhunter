package scripts.xHunter;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Login;
import org.tribot.api2007.Projection;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.EventBlockingOverride;
import org.tribot.script.interfaces.MessageListening07;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.RandomEvents;
import org.tribot.script.interfaces.EventBlockingOverride.OVERRIDE_RETURN;

import scripts.xHunter.GUI;
import scripts.xHunter.Utils.*;
import scripts.xHunter.Nodes.*;

@ScriptManifest(authors = { "xCode" }, category = "Hunter", name = "xHunter")
public class xHunter extends Script implements Painting, MessageListening07{
	
	private ArrayList<Node> nodes = new ArrayList<Node>();
	
	private boolean retrievedStats = false;
	private long startTime = 0;
	private int startingXP = 0;
	private int startingLevel = 0;
	private int monstersCaught = 0;
	private int currentXP = 0;
	private String status = "";
	
	private final Color transpBlackColor = new Color(0, 0, 0, 170);
	private final Color transpGreenColor = new Color(99, 209, 62, 170);
	private final Font textFont = new Font("Arial", 0, 13);
	private final Font textItalicFont = new Font("Arial", 2, 11);
	private final Font titleFont = new Font("Arial", 0, 18);
	
	public static final ABCUtil AntiBan = new ABCUtil();
	public static MonsterType monster;
	
	public static TrapFormation formation;
	public static boolean stop;
	public static boolean guiComplete = false;
	public static String[] valuables;

	@Override
	public void run() {
		GUI gui = new GUI();
		gui.setVisible(true);
		while(gui.isVisible())
		{
			General.sleep(100);
		}
		
		int dialogResult = JOptionPane.showConfirmDialog (null, "Due to a recent Runescape update randoms won't work. \r\n Woul you like to disable TriBot random solvers?","Disable randoms", JOptionPane.YES_NO_OPTION);
		if(dialogResult == JOptionPane.YES_OPTION)
		{
			setRandomSolverState(false);
		}
		
		initialize();
		
		while(!stop)
		{
			for (final Node n : nodes) {
				if (n.isValid()) {
					status = n.getClass().getSimpleName();
					n.execute();
				}
			}
			sleep(General.random(10, 50));
		}
		
	}
	
	/**
	 * Initializes all values and adds the nodes to the node list.
	 */
	private void initialize()
	{
		stop = false;
		status = "Init";
		General.useAntiBanCompliance(true);
		valuables = new String[monster.getValuableLoot().length + 1];
		for(int i = 0 ; i < monster.getValuableLoot().length ; i++)
			valuables[i] = monster.getValuableLoot()[i];
		valuables[valuables.length - 1] = monster.getTrap().getTrapName()[0];
		
		if(Login.getLoginState() == STATE.INGAME && !retrievedStats)
		{
			startingLevel = Skills.getActualLevel(SKILLS.HUNTER);
			startingXP = Skills.getXP(SKILLS.HUNTER);
			currentXP = startingXP;
			startTime = Timing.currentTimeMillis();
			retrievedStats = true;
		}
		nodes.add(new WalkToLocation());
		nodes.add(new SetupTrap());
		nodes.add(new PickupTrap());
		nodes.add(new GetTrap());
		nodes.add(new DropJunk());
		nodes.add(new Antiban());
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D) gg;
		
		currentXP = Skills.getXP(SKILLS.HUNTER);
		String runTime = Timing.msToString((Timing.currentTimeMillis() - startTime));
		int gainedXP = currentXP - startingXP;
		int currentLevel = Skills.getActualLevel(SKILLS.HUNTER);
		int gainedLevel = currentLevel - startingLevel;
		int percentToNextLevel = Skills.getPercentToNextLevel(SKILLS.HUNTER);
		double xpH = ((double)gainedXP * (3600000.0 / (Timing.currentTimeMillis() - startTime)));
		long ttnl = (long)(Skills.getXPToNextLevel(SKILLS.HUNTER) / xpH * 3600000);
		
		final int paintX = 30;
		final int paintY = 283;
		final int row2offsetX = 290;
		
		g.setColor(transpBlackColor);
		g.fillRect(3, 246, 513, 93);
		
		g.setColor(Color.white);
		
		g.setFont(titleFont);
		g.drawString("xHunter", 235, 265);
		
		g.setFont(textItalicFont);
		g.drawString("by xCode", 242, 280);
		
		g.setFont(textFont);
		
		g.drawString("Runtime: " + runTime, paintX, paintY - 15);
		g.drawString(monster.toString() + " caught: " + monstersCaught + " [" + (int)((double)monstersCaught * (3600000.0 / (Timing.currentTimeMillis() - startTime))) + " P/H]", paintX, paintY);
		g.drawString("XP gained: " + gainedXP + " [" + (int)xpH + " P/H]", paintX, paintY + 15);
		g.drawString("Level: " + currentLevel + " [+" + gainedLevel + "]", paintX, paintY + 30);
		
		g.drawString("Monster: " + monster.toString(), paintX + row2offsetX, paintY - 15);
		g.drawString("Location: None" , paintX + row2offsetX, paintY);
		g.drawString("Status: " + status, paintX + row2offsetX, paintY + 30);
		
		g.setColor(transpGreenColor);
		int greenBarWidth = (int) (513.0 * (percentToNextLevel / 100.0));
		g.fillRect(4, 322, greenBarWidth, 16);
		g.setColor(Color.DARK_GRAY);
		g.drawLine(4, 322, 517, 322);
		g.setColor(Color.WHITE);
		if(gainedXP > 0)
			g.drawString(percentToNextLevel + "% to " + (Skills.getActualLevel(SKILLS.HUNTER) + 1) + " Hunter [" + Timing.msToString(ttnl) + "]", 155, 335);
		
		RSTile[][] tileFormation = formation.getTileFormation();
		int formationX = 248;
		int formationY = 286;
		for(int i = 0 ; i < tileFormation.length ; i ++)
		{
			for( int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(tileFormation[i][j] != null)
				{
					g.setColor(transpGreenColor);
					g.fillPolygon(Projection.getTileBoundsPoly(new RSTile(monster.getSetupLocation().getX() - 1 + i, xHunter.monster.getSetupLocation().getY() - 1 + j), 1));
				}
				else
				{
					g.setColor(Color.white);
					g.drawPolygon(Projection.getTileBoundsPoly(new RSTile(xHunter.monster.getSetupLocation().getX() - 1 + i, xHunter.monster.getSetupLocation().getY() - 1 + j), 1));
				}
				g.fillRect(formationX + j * 12, formationY + i * 12, 10, 10);
				
				
			}
		}		
	}

	@Override
	public void clanMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void duelRequestReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void personalMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serverMessageReceived(String message) {
		if (message.toLowerCase().contains("you've caught")) 
            monstersCaught++;
		
	}

	@Override
	public void tradeRequestReceived(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
