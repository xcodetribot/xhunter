package scripts.xHunter.Utils;

import org.tribot.api.General;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public enum MonsterType {
	CRIMSON_SWIFT 		("Crimson swift", TrapType.BIRD_SNARE, new RSArea[] {new RSArea(new RSTile(2608, 2939, 0), new RSTile(2616, 2926, 0))}, new String[] {"Red feather"}),
	GREY_CHINCHOMPA		("Grey chin", TrapType.BOX_TRAP, new RSArea[] {new RSArea(new RSTile(2357, 3541, 0), new RSTile(2350, 3546, 0))}, new String[]{"Coins", "Chinchompa"}),
	RED_CHINCHOMPA 		("Red chin", TrapType.BOX_TRAP, new RSArea[] {new RSArea(new RSTile(2554, 2928, 0), new RSTile(2559, 2936, 0)), new RSArea(new RSTile(2556, 2917, 0), new RSTile(2560, 2914, 0))}, new String[]{"Coins", "Red chinchompa"}),
	TROPICAL_WAGTAIL 	("Tropical wagtail", TrapType.BIRD_SNARE, new RSArea[] {new RSArea(new RSTile(2529, 2939, 0), new RSTile(2523, 2932, 0))}, new String[] {"Stripy feather"});

	private String monsterName;
	private TrapType trap;
	private String[] valuableLoot;
	private RSArea[] locations;
	private RSArea chosenLocation;
	private RSTile setupLocation;
	
	MonsterType(String monsterName, TrapType trap, RSArea[] locations, String[] valuableLoot)
	{
		this.valuableLoot = valuableLoot;
		this.monsterName = monsterName;
		this.trap = trap;
		this.locations = locations;
	}
	
	/**
	 * Get the TrapType which is needed to catch this monster.
	 * @return TrapType object which represents the trap needed to catch this monster.
	 */
	public TrapType getTrap()
	{
		return trap;
	}
	
	/**
	 * Get the loot which this monster drops which is valuable.
	 * @return String array with the names of the valuables drops.
	 */
	public String[] getValuableLoot()
	{
		return valuableLoot;
	}
	
	/**
	 * Get the locations of where to find this monster.
	 * @return an array of RSArea which holds all the areas of the monster.
	 */
	public RSArea[] getLocations()
	{
		return locations;
	}
	
	/**
	 * Set the chosen location from the GUI based on the index in the array of the location array.
	 * @param index of the chosen location in the location array.
	 */
	public void setChosenLocation(int index)
	{
		if(index < locations.length && index >= 0)
			chosenLocation = locations[0];
		else
			General.println("Can't set chosen area.");
	}
	
	/**
	 * Get the chosen location.
	 * @return RSArea of the chosen location.
	 */
	public RSArea getChosenLocation()
	{
		return chosenLocation;
	}
	
	/**
	 * Get the location which represents the center where you set the traps.
	 * @return RSTile of the center location.
	 */
	public RSTile getSetupLocation()
	{
		return setupLocation;
	}
	
	/**
	 * Sets the setup location of where to place your traps.
	 * @param tile	of the center location of where to place traps.
	 */
	public void setSetupLocation(RSTile tile)
	{
		this.setupLocation = tile;
	}
	
	@Override
	public String toString()
	{
		return monsterName;
	}

}
