package scripts.xHunter.Utils;

import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSTile;

public abstract class Node {
	
	/**
     * Is action valid?
     * @return true if valid, false otherwise.
     */
    public abstract boolean isValid();
    
    /**
     * Execute action.
     */
    public abstract void execute();
    
}
