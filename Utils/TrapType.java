package scripts.xHunter.Utils;

public enum TrapType {
	BIRD_SNARE (new String[]{"Bird snare"}, "Lay", "Investigate", "Dismantle", "Check"),
	BOX_TRAP (new String[]{"Box trap", "Shaking box"}, "Lay", "Investigate", "Dismantle", "Check");
	private String[] trapName;
	
	private String setupOption;
	private String failedOption;
	private String dismantleOption;
	private String successOption;
	
	TrapType(String[] trapName, String setupOpt, String failedOpt, String dismantleOption, String successOpt)
	{
		this.trapName = trapName;
		this.setupOption = setupOpt;
		this.failedOption = failedOpt;
		this.dismantleOption = dismantleOption;
		this.successOption = successOpt;
	}
	
	/**
	 * Get the names of the traps that are being used.
	 * @return trapnames
	 */
	public String[] getTrapName()
	{
		return trapName;
	}
	
	/**
	 * Get the option that is being used to set-up a trap.
	 * @return the setup option.
	 */
	public String getSetupOption()
	{
		return setupOption;
	}
	
	/**
	 * Get the option that is available when a trap has fallen.
	 * @return failed option.
	 */
	public String getFailedOption()
	{
		return failedOption;
	}
	
	/**
	 * Get the option that is being used to dismantle a trap.
	 * @return dismantle option.
	 */
	public String getDismantleOption()
	{
		return dismantleOption;
	}
	
	/**
	 * Get the option that is available when a trap has caught something.
	 * @return succes option.
	 */
	public String getSuccessOption()
	{
		return successOption;
	}
}
