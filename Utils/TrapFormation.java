package scripts.xHunter.Utils;

import java.awt.Point;

import org.tribot.api.General;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xHunter.xHunter;
import scripts.xHunter.Nodes.GetTrap;

public enum TrapFormation 
{
	CROSS_FORMATION 	(new int[][] {
						{2, 0, 3}, 
						{0, 1, 0}, 
						{4, 0, 5}}),
	PLUS_FORMATION 		(new int[][] {
						{0, 2, 0}, 
						{3, 1, 4}, 
						{0, 5, 0}});
	
	private int[][] formation;
	private RSTile[][] tileFormation;
	private GetTrap getTrap;
	
	/**
	 * Constructor of TrapFormation.
	 * @param formation	matrix. 0 = nothing, >0 = trap
	 */
	TrapFormation(int[][] formation) // [x][y]
	{
		this.formation = formation;
		getTrap = new GetTrap();
		
		tileFormation = new RSTile[][] {
				{null, null, null}, 
				{null, null, null}, 
				{null, null, null}};
	}
	
	/**
	 * Get the tile formation.
	 * @return	matrix of the tile formation.
	 */
	public RSTile[][] getTileFormation()
	{
		return tileFormation;
	}
	
	/**
	 * Get the next available spot where we can place a trap.
	 * @return	RSTile of the spot which is available.
	 */
	public RSTile getNextAvailableSpot()
	{
		RSTile[] availableSpots = getAvailableSpots();
		RSTile closest = null;
		double closestDistance = Double.MAX_VALUE;
		
		for(int i = 0 ; i < availableSpots.length ; i++)
		{
			double dist = Player.getPosition().distanceToDouble(availableSpots[i]);
			if(dist < closestDistance)
			{
				closestDistance = dist;
				closest = availableSpots[i];
			}
		}
		
		return closest;
	}
	
	/**
	 * Get the tile by it's matrix coords.
	 * @param x	X-coord of the formation [x][y]
	 * @param y Y-coord of the formation [x][y]
	 * @return Tile of the coords in the matrix. When out of bounds, return null.
	 */
	public RSTile getTileByMatrixCoords(int x, int y)
	{
		if(x >= 0 && y >= 0 && x < formation.length && y < formation[x].length)
			return tileFormation[x][y];
		return null;
	}
	
	/**
	 * Set a specified tile to null when it's not null. Used when a trap has been removed.
	 * @param tile	the position of the trap.
	 */
	public void setTileToNull(RSTile tile)
	{
		for(int i = 0 ; i < tileFormation.length ; i ++)
		{
			for(int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(tileFormation[i][j] != null && tile != null && tileFormation[i][j].distanceTo(tile) == 0)
					tileFormation[i][j] = null;
			}
		}
	}
	
	/**
	 * Get the next tile to hover over. Prioritizes fallen traps.
	 * @return RSTile of the tile to hover over.
	 */
	public RSTile getNextHover()
	{
		RSGroundItem[] trapsOnGround = GroundItems.find(xHunter.monster.getTrap().getTrapName());
		if(trapsOnGround.length > 0)
		{
			return trapsOnGround[0].getPosition();
		}
		
		if(getTrap.isValid() && getTrap.getTrap() != null)
		{
			RSTile trapPos = getTrap.getTrap().getPosition();
			getTrap.setTrap(null);
			
			return trapPos;
		}
		
		RSTile nextAvailableSpot = getNextAvailableSpot();
		if(nextAvailableSpot != null)
		{
			return nextAvailableSpot;
		}
		
		return null;
	}
	
	/**
	 * Set the tile. Used when a trap is placed on the tile.
	 * @param tile	position of the trap.
	 */
	public void setTile(RSTile tile)
	{
		int x = tile.getX() - xHunter.monster.getSetupLocation().getX() + 1;
		int y = tile.getY() - xHunter.monster.getSetupLocation().getY() + 1;
		
		if(x >= 0 && y >= 0 && x < formation.length && y < formation[x].length)
		{
			tileFormation[x][y] = tile;
		}
	}
	
	/**
	 * Get all the available spots.
	 * @return	RSTile array of all available spots.
	 */
	public RSTile[] getAvailableSpots()
	{
		RSTile[] spots = new RSTile[]{};
		
		for(int i = 0 ; i < tileFormation.length ; i ++)
		{
			for(int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(formation[i][j] > 0 && formation[i][j] <= getMaxTraps() && tileFormation[i][j] == null)
					{
						RSTile[] tempSpots = new RSTile[spots.length + 1];
						for(int k = 0 ; k < spots.length ; k++)
						{
							tempSpots[k] = spots[k];
						}
						tempSpots[tempSpots.length - 1] = new RSTile(xHunter.monster.getSetupLocation().getX() + i - 1, xHunter.monster.getSetupLocation().getY() + j - 1);
						spots = tempSpots;
					}
			}
		}
		
		return spots;
	}
	
	/**
	 * Get the matrix coords as Point of the given number in the formation matrix.
	 * @param num is the number it has to correspond to in the matrix.
	 * @return Point, representing the matrix coords.
	 */
	public Point getMatrixCoords(int num)
	{
		for(int i = 0 ; i < formation.length ; i ++)
		{
			for(int j = 0 ; j < formation[i].length ; j++)
			{
				if(formation[i][j] == num)
					return new Point(i,j);
			}
		}
		
		return null;
	}
	
	/**
	 * Is a trap on the ground ours?
	 * @param trap	Position of the trap.
	 * @return true when the trap is ours, false otherwise.
	 */
	public boolean isOurs(RSTile trap)
	{
		for(int i = 0 ; i < tileFormation.length ; i ++)
		{
			for(int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(trap != null && tileFormation[i][j] != null && tileFormation[i][j].distanceTo(trap) == 0)
					return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Get the max amount of traps to lay.
	 * @return max amount of traps.
	 */
	public int getMaxTraps()
	{
		int huntingLevel = Skills.getActualLevel(SKILLS.HUNTER);
		
		if(huntingLevel >= 80)
			return 5;
		if(huntingLevel >= 60)
			return 4;
		if(huntingLevel >= 40)
			return 3;
		if(huntingLevel >= 20)
			return 2;
		return 1;
	}
	
	@Override
	public String toString()
	{
		String s = "";
		for(int i = 0 ; i < tileFormation.length ; i ++)
		{
			for(int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(tileFormation[i][j] == null)
					s += "[O]";
				else
					s += "[X]";
			}
			s += "\n";
		}
		
		return s;
	}
	
	/**
	 * Check the states of all the traps we have layed.
	 */
	public void checkTraps()
	{
		RSObject[] traps = Objects.findNearest(5, xHunter.monster.getTrap().getTrapName());
		for(int i = 0 ; i < tileFormation.length ; i++)
		{
			for(int j = 0 ; j < tileFormation[i].length ; j++)
			{
				if(tileFormation[i][j] != null)
				{
					boolean isThere = false;
					for(int k = 0 ; k < traps.length ; k++)
					{
						if(tileFormation[i][j].distanceTo(traps[k].getPosition()) == 0)
							isThere = true;
					}
					
					if(!isThere)
						tileFormation[i][j] = null;
				}
			}
		}
	}
	
}
