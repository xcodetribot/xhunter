package scripts.xHunter.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.DynamicMouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.CustomRet_0P;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class SetupTrap extends Node{

	@Override
	public boolean isValid() {
		xHunter.formation.checkTraps();
		return xHunter.formation.getNextAvailableSpot() != null && Inventory.find(xHunter.monster.getTrap().getTrapName()).length > 0;
	}

	@Override
	public void execute() {
		final RSTile spotToUse = xHunter.formation.getNextAvailableSpot();
		if(spotToUse == null)
			return;
		
		if(Player.getPosition().distanceTo(spotToUse) != 0)
			if(!Walking.clickTileMS(spotToUse, "Walk here"))
				return;
		
		General.sleep(300, 400);
		Timing.waitCondition(new Condition() {
			
			@Override
			public boolean active() {
				return Player.getPosition().distanceTo(spotToUse) == 0 || !Player.isMoving();
			}
		}, General.random(2500, 3000));
		if(Player.getPosition().distanceTo(spotToUse) != 0)
			return;
		
		RSItem[] traps = Inventory.find(xHunter.monster.getTrap().getTrapName());
		if(traps.length > 0)
		{
			if(traps[0].click(xHunter.monster.getTrap().getSetupOption()))
			{
				xHunter.formation.setTile(spotToUse);
				RSTile next = xHunter.formation.getNextHover();
				if(next != null)
				{
					if(xHunter.AntiBan.BOOL_TRACKER.HOVER_NEXT.next())
					{
						General.sleep(xHunter.AntiBan.DELAY_TRACKER.ITEM_INTERACTION.next());
						next.hover();
					}
				}
			}
			
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					RSObject[] traps = Objects.find(1, xHunter.monster.getTrap().getTrapName());
					for(int i = 0 ; i < traps.length ; i++)
						if(traps[i].getPosition().distanceTo(spotToUse) == 0)
						{
							return true;
						}
					return false;
				}
			}, General.random(3000, 3500));
		}
	}

}
