package scripts.xHunter.Nodes;

import org.tribot.api.General;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class DropJunk extends Node{

	@Override
	public boolean isValid() {
		RSItem[] inventoryItems = Inventory.getAll();
		for(int i = 0 ; i < inventoryItems.length ; i++)
		{
			boolean itemIsJunk = true;
			for(int j = 0 ; j < xHunter.valuables.length ; j++)
			{
				if(inventoryItems[i].getDefinition().getName().equalsIgnoreCase(xHunter.valuables[j]))
					itemIsJunk = false;
			}
			if(itemIsJunk)
				return itemIsJunk && xHunter.formation.getNextAvailableSpot() == null;
		}
		
		return Inventory.isFull();
	}

	@Override
	public void execute() {
		Inventory.dropAllExcept(xHunter.valuables);
		
		General.sleep(200, 600);
	}

}
