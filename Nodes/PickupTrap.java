package scripts.xHunter.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSTile;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class PickupTrap extends Node{

	@Override
	public boolean isValid() {
		return GroundItems.findNearest(xHunter.monster.getTrap().getTrapName()).length > 0;
	}

	@Override
	public void execute() {
		RSGroundItem[] groundItems = GroundItems.findNearest(xHunter.monster.getTrap().getTrapName());
		if(groundItems.length > 0)
		{
			General.sleep(xHunter.AntiBan.DELAY_TRACKER.NEW_OBJECT.next());
			if(DynamicClicking.clickRSGroundItem(groundItems[0], "Take"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return GroundItems.findNearest(xHunter.monster.getTrap().getTrapName()).length == 0;
					}
				}, General.random(3000, 3500));	
				General.sleep(xHunter.AntiBan.DELAY_TRACKER.ITEM_INTERACTION.next());
			}
		}
		
	}

}
