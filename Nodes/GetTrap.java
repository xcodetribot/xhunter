package scripts.xHunter.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Objects;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class GetTrap extends Node{
	
	private RSObject trap = null;
	private boolean isSuccess = false;

	@Override
	public boolean isValid() {
		RSObject[] traps = Objects.findNearest(6, xHunter.monster.getTrap().getTrapName());
		for(int i = 0 ; i < traps.length ; i++)
		{
			if(xHunter.formation.isOurs(traps[i].getPosition()))
			{
				String[] availableActions = traps[i].getDefinition().getActions();
				if(stringArrayContains(availableActions, xHunter.monster.getTrap().getSuccessOption()))
				{
					isSuccess = true;
					trap = traps[i];
					return true;
				}
				if(!stringArrayContains(availableActions, xHunter.monster.getTrap().getFailedOption()))
				{
					isSuccess = false;
					trap = traps[i];
					return true;
				}
					
			}
		}
		return false;
	}

	@Override
	public void execute() 
	{
		if(trap != null)
		{
			final RSTile trapLocation = trap.getPosition();
			if(DynamicClicking.clickRSObject(trap, xHunter.monster.getTrap().getSuccessOption()) || DynamicClicking.clickRSObject(trap, xHunter.monster.getTrap().getDismantleOption()))
			{				
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						RSObject[] trapsLayed = Objects.findNearest(6, xHunter.monster.getTrap().getTrapName());
						
						for(int i = 0 ; i < trapsLayed.length ; i++)
						{
							if(trapsLayed[i].getPosition().distanceTo(trapLocation) == 0)
								return false;
						}
						return true;
						}
				}, General.random(1300, 1500));
				
				RSObject[] trapsLayed = Objects.findNearest(6, xHunter.monster.getTrap().getTrapName());
				boolean isRemoved = true;
				
				for(int i = 0 ; i < trapsLayed.length ; i++)
				{
					if(trapsLayed[i].getPosition().distanceTo(trapLocation) == 0)
						isRemoved = false;
				}
				
				if(isRemoved)
				{
					xHunter.formation.setTileToNull(trapLocation);
					xHunter.formation.checkTraps();
					General.sleep(xHunter.AntiBan.DELAY_TRACKER.ITEM_INTERACTION.next());
				}
				trap = null;
			}
		}
	}
	
	private boolean stringArrayContains(String[] array, String str)
	{
		for(int i = 0 ; i < array.length ; i++)
			if(array[i].equalsIgnoreCase(str))
				return true;
		return false;
	}
	
	public RSObject getTrap()
	{
		return trap;
	}
	
	public void setTrap(RSObject trap)
	{
		this.trap = trap;
	}

}
