package scripts.xHunter.Nodes;

import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class WalkToLocation extends Node{

	@Override
	public boolean isValid() {
		return xHunter.monster.getSetupLocation() != null && xHunter.monster.getSetupLocation().distanceTo(Player.getPosition()) >= 4;
	}

	@Override
	public void execute() {
		WebWalking.walkTo(xHunter.monster.getSetupLocation());
	}

}
