package scripts.xHunter.Nodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.xHunter.xHunter;
import scripts.xHunter.Utils.Node;

public class Antiban extends Node{

	@Override
	public boolean isValid() {
		return (Player.getAnimation() == -1 || Player.isMoving()) && xHunter.formation.getNextAvailableSpot() == null;
	}

	@Override
	public void execute() {
		xHunter.AntiBan.performCombatCheck();

		xHunter.AntiBan.performEquipmentCheck();

		xHunter.AntiBan.performExamineObject();

		xHunter.AntiBan.performFriendsCheck();

		xHunter.AntiBan.performLeaveGame();

		xHunter.AntiBan.performMusicCheck();

		xHunter.AntiBan.performPickupMouse();

		xHunter.AntiBan.performQuestsCheck();

		xHunter.AntiBan.performRandomMouseMovement();

		xHunter.AntiBan.performRandomRightClick();

		xHunter.AntiBan.performRotateCamera();

		xHunter.AntiBan.performTimedActions(SKILLS.HUNTER);

		xHunter.AntiBan.performXPCheck(SKILLS.HUNTER);
		
		if(Player.isMoving() && Game.getRunEnergy() >= xHunter.AntiBan.INT_TRACKER.NEXT_RUN_AT.next())
		{
			Options.setRunOn(true);
			xHunter.AntiBan.INT_TRACKER.NEXT_RUN_AT.reset();
		}
		
	}

}
